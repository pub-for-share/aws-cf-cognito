create stack
aws cloudformation create-stack --stack-name 'test-cognito' --template-body file://cognito.yml && aws cloudformation wait stack-create-complete --stack-name 'test-cognito'

store the pool id to a bash var
userpoolid=$(aws cloudformation describe-stacks --stack-name 'test-cognito' --query 'Stacks[].Outputs[?OutputKey==`UserPoolID`].[OutputValue]' --output text)

store the client app id to a bash var
appclientid=$(aws cloudformation describe-stacks --stack-name 'test-cognito' --query 'Stacks[].Outputs[?OutputKey==`AppClientID`].[OutputValue]' --output text)

create a test user
aws cognito-idp sign-up --region ap-southeast-1 --client-id $appclientid --username denyip@msn.com --password Passw0rd!

verify the test user
aws cognito-idp admin-confirm-sign-up --region ap-southeast-1 --user-pool-id $userpoolid --username denyip@msn.com
